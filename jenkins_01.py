import requests
import traceback
from requests.auth import HTTPBasicAuth
from pprint import pprint
from openpyxl import Workbook
from datetime import datetime


def main():
    login = 'nikitos0'
    password = 'admin4all'
    jenkins_url = 'http://sbt-ouiefs-036.sigma.sbrf.ru/jenkins/%s'

    auth = HTTPBasicAuth(login, password)

    ok_status_codes = [200, ]

    try:
        req = requests.get(url=jenkins_url % ('/api/json?tree=jobs[name,url]&pretty=true'), auth=auth)
        print(req.status_code)

        assert (req.status_code in ok_status_codes), 'invalid status code'

        jobs = req.json()['jobs']

        try:

            wb = Workbook()
            ws = wb.active
            ws.append(['job_name', 'job_url', 'date'])

            for job in jobs:
                print(job['name'])
                ws.append([job['name'], job['url'], datetime.now()])

            wb.save('test.xlsx')
        finally:
            wb.close()


    except requests.exceptions.ConnectionError:
        print('invalid url')

    except AssertionError as E:
        print(str(E))

    except Exception as E:
        print('Exception')


        # traceback.print_exc()




if __name__ == '__main__':
    main()
